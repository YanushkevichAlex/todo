import React, { Component } from 'react';
import AppHeader from '../app-header/app-header';
import SearchPanel from '../search-panel/search-panel';
import TodoList from '../todo-list/todo-list';
import ItemStatusFilter from '../item-status-filter/item-status-filter';
import AddItem from '../add-item/add-item';
import './app.css';


export default class App extends  Component {

    maxId = 100;

    state = {
        todoData:  [
            this.createTodoItem('Drink Coffee'),
            this.createTodoItem('Make Awesome App'),
            this.createTodoItem('Have a lunch')
        ],
        status: 'all',
        searchText: ''
    };

    createTodoItem(label) {
        return {
            label,
            important: false,
            id: this.maxId++,
            done: false
        };
    }

    deleteItem = (id) => {
        this.setState(({ todoData }) => {
            const idx = todoData.findIndex((el) => el.id === id);
            const result = [...todoData.slice(0, idx), ...todoData.slice(idx+1)];
            return {
                todoData: result
            }
        });
    };

    addItem = (text) => {
        const newItem = this.createTodoItem(text);
        this.setState(({ todoData }) => {
            const newData = [...todoData, newItem];
            return {
                todoData: newData
            }
        });
    };

    toggleProperty(arr, id, propName) {
        const idx = arr.findIndex((el) => el.id === id);
        const oldItem = arr[idx];
        const newItem = { ...oldItem, [propName]: !oldItem[propName] };
        return [...arr.slice(0, idx), newItem, ...arr.slice(idx+1)];
    }

    onToggleDone = (id) => {
        this.setState(({ todoData }) => {
            return {
                todoData: this.toggleProperty(todoData,id,'done')
            }
        });
    };

    onToggleImportant = (id) => {
        this.setState(({ todoData }) => {
            return {
                todoData: this.toggleProperty(todoData,id,'important')
            }
        });
    };

    changeStatus = (status) => {
        this.setState({
            status
        });
    };

    changeSearchText = (searchText) => {
        this.setState({
            searchText
        });
    };

    filteredTodoList = () => {
        const { todoData, searchText, status } = this.state;
        if (status === 'all') {
            return todoData.filter(item => item.label.toLowerCase().indexOf(searchText.toLowerCase()) !== -1);
        } else {
            if (status === 'active') {
                return todoData.filter((item) => !item.done && item.label.toLowerCase().indexOf(searchText.toLowerCase()) !== -1);
            } else {
                return todoData.filter((item) => item.done && item.label.toLowerCase().indexOf(searchText.toLowerCase()) !== -1);
            }
        }
    };

    render() {
        const { todoData } = this.state;
        const doneCount = todoData.filter((item) => item.done).length;
        const toDoCount = todoData.length - doneCount;
        const todoDataList = this.filteredTodoList();
        return (
            <div className="todo-app">
                <AppHeader toDo={toDoCount} done={doneCount}/>
                <div className="top-panel d-flex">
                    <SearchPanel searchText={this.changeSearchText}/>
                    <ItemStatusFilter changeStatus={this.changeStatus} status={this.state.status}/>
                </div>
                <TodoList todos={todoDataList}
                          onDeleted={(id) => this.deleteItem(id)}
                          onToggleImportant={this.onToggleImportant}
                          onToggleDone={this.onToggleDone} />
                <AddItem onAdd={this.addItem} />
            </div>
        );
    }
};