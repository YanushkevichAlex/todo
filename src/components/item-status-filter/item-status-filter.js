import React, {Component} from 'react';


export default class ItemStatusFilter extends Component {


    onPressAll = () => {
      this.props.changeStatus('all')
    };

    onPressActive = () => {
        this.props.changeStatus('active')
    };

    onPressDone = () => {
        this.props.changeStatus('done')
    };

    render() {

        const { status } = this.props;

        let classNameAll = `btn + ${status === 'all' ? 'btn-info' : 'btn-outline-secondary'}`;
        let classNameActive = `btn + ${status === 'active' ? 'btn-info' : 'btn-outline-secondary'}`;
        let classNameDone = `btn + ${status === 'done' ? 'btn-info' : 'btn-outline-secondary'}`;

        return (
            <div className="btn-group">
                <button type="button"
                        className={classNameAll} onClick={this.onPressAll}>All</button>
                <button type="button"
                        className={classNameActive} onClick={this.onPressActive}>Active</button>
                <button type="button"
                        className={classNameDone} onClick={this.onPressDone}>Done</button>
            </div>
        );
    }
}