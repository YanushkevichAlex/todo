import React, {Component} from 'react';
import './search-panel.css';

export default class SearchPanel extends Component {

    changeSearchText = (event) => {
        this.props.searchText(event.target.value);
    };

    render() {
        return (
            <input type="text"
                   className="form-control search-input"
                   placeholder="type to search"
                   onChange={this.changeSearchText}/>
        );
    }
};
